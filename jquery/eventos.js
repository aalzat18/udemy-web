$(document).ready(function () {

    var boton = $("#boton");

    //boton.click(function () {alert();});

    function saludo() {
        alert();
    }

    function fondo1() {
        $("body").css({
            background: "#000000"
        });
    }

    function fondo2() {
        $("body").css({
            background: "#ffffff"
        });
    }

    //boton.on("click", saludo);
    //boton.click(saludo);

    boton.on("mouseenter", fondo1);
    boton.on("mouseleave", fondo2);

    //eliminando eventos
    boton.on("click", function () {
        alert();
    })

    $("#desactivar").on("click", function () {
        boton.off("click");
    });

    $("a").on("click", function (e) {
        e.preventDefault();
    });
});