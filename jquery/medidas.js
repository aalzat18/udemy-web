$(document).ready(function () {
    var titulo = $("#titulo");
    var info = $("#info");

    //ancho del elemento
    info.append("Ancho: " + titulo.width() + "<br />");
    //ancho + padding
    info.append("Ancho: " + titulo.innerWidth() + "<br />");
    //ancho + padding + border
    info.append("Ancho: " + titulo.outerWidth() + "<br />");
    //ancho + padding + border + margin
    info.append("Ancho: " + titulo.outerWidth(true) + "<br />");
    //alto del elemento
    info.append("Alto: " + titulo.height() + "<br />");
    //alto + padding
    info.append("Alto: " + titulo.innerHeight() + "<br />");
    //alto + padding + border
    info.append("Alto: " + titulo.outerHeight() + "<br />");
    //alto + padding + border + margin
    info.append("Alto: " + titulo.outerHeight(true) + "<br />");
});