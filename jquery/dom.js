$(document).ready(function () {
    
    //parent or parents
    $("#tercera").parent().css({
        background: "#1b3d82"
    });

    //children
    //$("#padre").children("#tercera").fadeOut(2000);

    //find
    //$("#contenedor").find("div.caja").slideUp(3000);

    //Siblings
    //$("#tercera").siblings().fadeOut(1500);

    //Next or preview (prev)
    //$("#tercera").prevAll().css({background: "#000000"});

    //-----------------------------------

    //filtrar
    //$(".caja").first().css({background: "#000000"});
    //$(".caja").last().css({background: "#000000"});
    //$(".caja").eq(2).css({background: "#000000"});
    //$(".caja").filter("#tercera").css({background: "#000000"});
    $(".caja").not("#tercera").css({background: "#000000"});
});